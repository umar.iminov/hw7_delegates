﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
namespace OTUS_HW7_DELEGATES
{
    class Program
    {
        static void Main()
        { 
            
            string targetDirectory = Path.Combine(Directory.GetCurrentDirectory(), Configuration.GetTargetDirectory());
            Console.WriteLine($"targetDirectory = {targetDirectory}");
            List<string> fileList = new() { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
            int waitingInterval = Configuration.GetWaitingInterval();
            Console.WriteLine($"watiniginterval = {waitingInterval}");
            if (!Directory.Exists(targetDirectory))
                Directory.CreateDirectory(targetDirectory);

            using DocumentsReceiver documentsReceiver = new();
            documentsReceiver.DocumentsReady += DocumentsReadyEvent;
            documentsReceiver.TimedOut += TimedOutEvent;
            
            Console.WriteLine("Введите 1 для автосоздания файлов");
            var test = Console.ReadLine();
            
            bool isStarted = documentsReceiver.Start(targetDirectory, waitingInterval, fileList);


            if (!isStarted)
            {
                Console.WriteLine("Прием документов не был запущен!");
                return;
            }
            
            if (test == "1")
            {
                Thread.Sleep(waitingInterval/2);
                MakeTestFiles(targetDirectory, fileList);
            }
            Console.ReadLine();
        }

        private static void DocumentsReadyEvent()
        {
            Console.WriteLine("Документы получены. Приём остановлен");
        }

        private static void TimedOutEvent()
        {
            Console.WriteLine("Превышение ожидания. Документы не поступили");
        }

        private static void MakeTestFiles(string path, List<string> fileList)
        {  
            foreach (string  fileName  in fileList)
            {
                File.Delete(Path.Combine(path, fileName));
                File.Create(Path.Combine(path, fileName));
            }
        }

        
     }
}
