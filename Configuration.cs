﻿using System;
using System.Configuration;

namespace OTUS_HW7_DELEGATES
{
    public static class Configuration
    {
        public static string GetTargetDirectory()
        {
            return ConfigurationManager.AppSettings.Get("TargetDirectory") ?? SetTargetDirectory();
        }

        public static int GetWaitingInterval()
        {
            if (!int.TryParse(ConfigurationManager.AppSettings.Get("WaitingInterval"), out int result))
                result = SetWaitingInterval();
            return result;

        }

        private static string SetTargetDirectory()
        { while (true) 
            {
                Console.WriteLine("Введите имя директории для документов.");
                string targetDirectory = Console.ReadLine();
                if (targetDirectory.Length==0)
                {
                    Console.WriteLine("Введено недопустимое значение.");
                    continue;
                }

                //ConfigurationManager.AppSettings.Set("TargetDirectory", targetDirectory);
                AddUpdateAppSettings("TargetDirectory", targetDirectory);
                return targetDirectory;
            }
        }

        private static int SetWaitingInterval()
        {
            while (true)
            {
                Console.WriteLine("Введите интервал ожидания в миллисекундах, число больше нуля");
                if (!int.TryParse(Console.ReadLine(), out int waitingInterval)|| waitingInterval==0)
                {
                    Console.WriteLine("Введено недопустимое значение");
                    continue;
                }
                //ConfigurationManager.AppSettings.Set("WaitingInterval", waitingInterval.ToString());
                AddUpdateAppSettings("WaitingInterval", waitingInterval.ToString());


                return waitingInterval;
            }
            
        }
        static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

    }
}
